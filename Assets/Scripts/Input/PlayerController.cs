using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private JoystickController joystickController;
    
    private Rigidbody playerRigidbody;
    public Canvas canvas;
    
    private Vector3 startPosition;
    private float forsage = 1f;
    private int reverse = 1;

    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float turnSpeed = 75f;
    [SerializeField] private GameObject frontLeftWheel;
    [SerializeField] private GameObject frontRightWheel;

    private void Awake()
    {
        joystickController = GetComponent<JoystickController>();
        playerRigidbody = GetComponent<Rigidbody>();
        playerInputActions = new PlayerInputActions();
        
        startPosition = transform.position;
        canvas.enabled = false;
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        playerInputActions.Kart.ChangeControls.performed += context => ChangeControls();
        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();
        
        playerInputActions.Kart.Forsage.started += context => ForsageOn();
        playerInputActions.Kart.Forsage.canceled += context => ForsageOff();

        float moveDirection = playerInputActions.Kart.Move.ReadValue<float>();
        Move(moveDirection);

        float turnAngle = playerInputActions.Kart.Turn.ReadValue<float>();
        Turn(turnAngle, moveDirection);
        WheelsTurn(turnAngle);
    }

    private void Move(float direction)
    {
        playerRigidbody.transform.Translate(Vector3.forward * direction * moveSpeed * forsage * Time.fixedDeltaTime);
    }

    private void Turn(float angle, float move)
    {
        if (move < 0)
            reverse = -1;
        else
            reverse = 1;

        if (move != 0)
            playerRigidbody.transform.Rotate(Vector3.up, angle * turnSpeed * forsage * reverse * Time.fixedDeltaTime);
    }

    public void WheelsTurn(float angle)
    {
        if (angle != 0)
        {
            frontLeftWheel.transform.Rotate(Vector3.up, angle * 45 * Time.fixedDeltaTime);
            frontRightWheel.transform.Rotate(Vector3.up, angle * 45 * Time.fixedDeltaTime);
        }
        else
        {
            frontLeftWheel.transform.rotation = transform.rotation;
            frontRightWheel.transform.rotation = transform.rotation;
        }
    }

    private void ResetPosition()
    {
        playerRigidbody.MovePosition(startPosition);
        playerRigidbody.MoveRotation(Quaternion.identity);
    }

    private void ForsageOn()
    {
        forsage = 3f;
    }

    private void ForsageOff()
    {
        forsage = 1f;
    }

    private void ChangeControls()
    {
        if (joystickController.enabled == true)
        {
            joystickController.enabled = false;
            canvas.enabled = false;
        }
        else
        {
            joystickController.enabled = true;
            canvas.enabled = true;
        }
    }
}

