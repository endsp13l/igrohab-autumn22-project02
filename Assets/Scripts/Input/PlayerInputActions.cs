//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.4.1
//     from Assets/Scripts/Input/PlayerInputActions.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @PlayerInputActions : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Kart"",
            ""id"": ""6ae0f8bd-a09d-4fef-89f5-d83e88b3d590"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""37a57f81-3556-444f-a914-f41a33e1049d"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""Turn"",
                    ""type"": ""Value"",
                    ""id"": ""330fbc76-bd90-4e97-ac1d-c1fc0ace7853"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""ResetPosition"",
                    ""type"": ""Button"",
                    ""id"": ""62180adc-db91-442d-b4b1-1f5631e9103b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Forsage"",
                    ""type"": ""Value"",
                    ""id"": ""d73f9b04-c61d-44ab-b07a-122c42f24bbe"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""ChangeControls"",
                    ""type"": ""Button"",
                    ""id"": ""9c10651e-f1c9-4abb-8ad8-a7c895525b31"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Joystick"",
                    ""type"": ""Value"",
                    ""id"": ""e89ef6a3-7cfa-4e89-a981-1b2c7a9cee11"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Arrows Acselerate/Brake"",
                    ""id"": ""1cf9769c-e975-4444-ad9e-06b475ad6446"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""e3c5293e-8e5d-4c33-be12-da94be1a2b2c"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""761b5bbd-0ead-4ce1-a38e-d5f07e3f2ac2"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""WASD Acselerate/Brake"",
                    ""id"": ""a3668475-7b15-4e31-b2f5-eb609471fd2d"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""7e55c987-eab1-45bf-897b-a64490b8ac5c"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""2481ae10-a0e8-4860-97a9-5fa84713022c"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""cc7cbeaf-5899-48fd-b450-9783ecff5932"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""ResetPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2a49c67b-bcb1-4e00-95ea-712c2881b380"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""ChangeControls"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee3bc2ed-17b6-4e1e-8c85-340d9d2028c4"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Forsage"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis Up/Down"",
                    ""id"": ""d4385df5-fffe-4344-bc32-19ba81e75093"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Joystick"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""b4626701-1350-4b3e-aca0-6c4880204115"",
                    ""path"": ""<Joystick>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Joystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""877a3bdb-989b-42da-b5cc-3bb342bbb47d"",
                    ""path"": ""<Joystick>/stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Joystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis Left/Right"",
                    ""id"": ""fb73d385-23d0-4f05-ab30-f44009516cee"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Joystick"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f0895e40-175a-42a6-b433-54914319ce2d"",
                    ""path"": ""<Joystick>/stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Joystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""509bf972-acb9-455c-b17e-a52711b2ce2e"",
                    ""path"": ""<Joystick>/stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Joystick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""WASD turns"",
                    ""id"": ""7053b04e-f9d2-490b-b387-07ddbb32cc18"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""d7231778-16af-4069-860e-1a76d2e62cf1"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5aa9a25e-b4d7-45a7-b196-1a8311c6ca8e"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows turns"",
                    ""id"": ""f6065ce1-35a6-4cf9-941c-dd2b84846164"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ec082da4-2e6a-439e-9468-c5357e4d88d4"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""f67aa167-cb53-40a2-aa83-ac08ca07fe13"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Turn"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PC"",
            ""bindingGroup"": ""PC"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Joystick>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Kart
        m_Kart = asset.FindActionMap("Kart", throwIfNotFound: true);
        m_Kart_Move = m_Kart.FindAction("Move", throwIfNotFound: true);
        m_Kart_Turn = m_Kart.FindAction("Turn", throwIfNotFound: true);
        m_Kart_ResetPosition = m_Kart.FindAction("ResetPosition", throwIfNotFound: true);
        m_Kart_Forsage = m_Kart.FindAction("Forsage", throwIfNotFound: true);
        m_Kart_ChangeControls = m_Kart.FindAction("ChangeControls", throwIfNotFound: true);
        m_Kart_Joystick = m_Kart.FindAction("Joystick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // Kart
    private readonly InputActionMap m_Kart;
    private IKartActions m_KartActionsCallbackInterface;
    private readonly InputAction m_Kart_Move;
    private readonly InputAction m_Kart_Turn;
    private readonly InputAction m_Kart_ResetPosition;
    private readonly InputAction m_Kart_Forsage;
    private readonly InputAction m_Kart_ChangeControls;
    private readonly InputAction m_Kart_Joystick;
    public struct KartActions
    {
        private @PlayerInputActions m_Wrapper;
        public KartActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Kart_Move;
        public InputAction @Turn => m_Wrapper.m_Kart_Turn;
        public InputAction @ResetPosition => m_Wrapper.m_Kart_ResetPosition;
        public InputAction @Forsage => m_Wrapper.m_Kart_Forsage;
        public InputAction @ChangeControls => m_Wrapper.m_Kart_ChangeControls;
        public InputAction @Joystick => m_Wrapper.m_Kart_Joystick;
        public InputActionMap Get() { return m_Wrapper.m_Kart; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(KartActions set) { return set.Get(); }
        public void SetCallbacks(IKartActions instance)
        {
            if (m_Wrapper.m_KartActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_KartActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnMove;
                @Turn.started -= m_Wrapper.m_KartActionsCallbackInterface.OnTurn;
                @Turn.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnTurn;
                @Turn.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnTurn;
                @ResetPosition.started -= m_Wrapper.m_KartActionsCallbackInterface.OnResetPosition;
                @ResetPosition.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnResetPosition;
                @ResetPosition.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnResetPosition;
                @Forsage.started -= m_Wrapper.m_KartActionsCallbackInterface.OnForsage;
                @Forsage.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnForsage;
                @Forsage.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnForsage;
                @ChangeControls.started -= m_Wrapper.m_KartActionsCallbackInterface.OnChangeControls;
                @ChangeControls.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnChangeControls;
                @ChangeControls.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnChangeControls;
                @Joystick.started -= m_Wrapper.m_KartActionsCallbackInterface.OnJoystick;
                @Joystick.performed -= m_Wrapper.m_KartActionsCallbackInterface.OnJoystick;
                @Joystick.canceled -= m_Wrapper.m_KartActionsCallbackInterface.OnJoystick;
            }
            m_Wrapper.m_KartActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Turn.started += instance.OnTurn;
                @Turn.performed += instance.OnTurn;
                @Turn.canceled += instance.OnTurn;
                @ResetPosition.started += instance.OnResetPosition;
                @ResetPosition.performed += instance.OnResetPosition;
                @ResetPosition.canceled += instance.OnResetPosition;
                @Forsage.started += instance.OnForsage;
                @Forsage.performed += instance.OnForsage;
                @Forsage.canceled += instance.OnForsage;
                @ChangeControls.started += instance.OnChangeControls;
                @ChangeControls.performed += instance.OnChangeControls;
                @ChangeControls.canceled += instance.OnChangeControls;
                @Joystick.started += instance.OnJoystick;
                @Joystick.performed += instance.OnJoystick;
                @Joystick.canceled += instance.OnJoystick;
            }
        }
    }
    public KartActions @Kart => new KartActions(this);
    private int m_PCSchemeIndex = -1;
    public InputControlScheme PCScheme
    {
        get
        {
            if (m_PCSchemeIndex == -1) m_PCSchemeIndex = asset.FindControlSchemeIndex("PC");
            return asset.controlSchemes[m_PCSchemeIndex];
        }
    }
    public interface IKartActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnTurn(InputAction.CallbackContext context);
        void OnResetPosition(InputAction.CallbackContext context);
        void OnForsage(InputAction.CallbackContext context);
        void OnChangeControls(InputAction.CallbackContext context);
        void OnJoystick(InputAction.CallbackContext context);
    }
}
