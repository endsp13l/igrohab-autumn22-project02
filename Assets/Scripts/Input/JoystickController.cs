using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class JoystickController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private PlayerController playerController;

    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] public FixedJoystick _joystick;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _turnSpeed;

    private float forsage = 1f;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        _rigidbody = GetComponent<Rigidbody>();
        playerController = GetComponent<PlayerController>();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        playerInputActions.Kart.Forsage.started += context => ForsageOn();
        playerInputActions.Kart.Forsage.canceled += context => ForsageOff();

        _rigidbody.transform.Translate(Vector3.forward * _joystick.Vertical * _moveSpeed * forsage * Time.fixedDeltaTime);
        _rigidbody.transform.Rotate(Vector3.up, _joystick.Horizontal * _turnSpeed * forsage * Time.fixedDeltaTime);

        if (_joystick.Horizontal != 0)
            playerController.WheelsTurn(_joystick.Horizontal * 75);
    }

    private void ForsageOn()
    {
        forsage = 3f;
    }

    private void ForsageOff()
    {
        forsage = 1f;
    }
}

